package br.ucsal.poo20172.bd2.revisao01.domain;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.ucsal.poo20172.bd2.revisao01.converters.EstadoCivilConverter;
import br.ucsal.poo20172.bd2.revisao01.enums.EstadoCivilEnum;

@Entity
public class Aluno {

	@Id
	private Integer id;

	private String nome;

	// @Enumerated(EnumType.STRING)
	@Convert(converter = EstadoCivilConverter.class)
	private EstadoCivilEnum estadoCivil;

	// Origem To Destino
	// MUITOS da origem (Aluno) para UM do destino (curso)
	// Existem muitos alunos em um curso
	// Mas s� existe um curso para um aluno
	// Mapemaneto unidirecional SE N�O for feito um mapeamento inverso na entidade Curso.
	@ManyToOne
	private Curso curso;

	public Aluno() {
	}

	public Aluno(Integer id, String nome, EstadoCivilEnum estadoCivil, Curso curso) {
		super();
		this.id = id;
		this.nome = nome;
		this.estadoCivil = estadoCivil;
		this.curso = curso;
	}

	@Override
	public String toString() {
		return "Aluno [id=" + id + ", nome=" + nome + ", estadoCivil=" + estadoCivil + ", curso=" + curso + "]";
	}

}
