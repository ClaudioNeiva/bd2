package br.ucsal.poo20172.bd2.revisao01.exemplo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

import br.ucsal.poo20172.bd2.revisao01.domain.Aluno;
import br.ucsal.poo20172.bd2.revisao01.domain.Curso;
import br.ucsal.poo20172.bd2.revisao01.enums.EstadoCivilEnum;

public class Exemplo01 {

	public static void main(String[] args) {
		EntityManager em = null;
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("revisao");
			em = emf.createEntityManager();
			// manipularCurso(em);
			manupilarAluno(em);
		} catch (Exception e) {
			logger.error("Problema grave na aplica��o: ", e);
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}

	private static void manupilarAluno(EntityManager em) {
		em.getTransaction().begin();
		Aluno aluno1 = new Aluno(1, "Claudio", EstadoCivilEnum.CASADO, null);
		em.persist(aluno1);
		em.getTransaction().commit();

	}

	private static void manipularCurso(EntityManager em) {
		em.getTransaction().begin();

		// Criamos um objeto que ainda n�o � gerenciado pelo EntityManager
		Curso curso = new Curso("INF", "Informatica");

		// Tornamos o objeto gerenciado pelo EntityManager, mas ainda n�o ser� feito o insert
		em.persist(curso);

		// O flush for�a o disparo dos sqls retidos pelo EntityManager.
		// em.flush();

		// O insert s� ser� feito no commit
		// Para evitar prender os objetos de banco por muito tempo, o hibernate mantem um controle dos comandos SQL que s�o necess�rios e gerencia o momento do
		// disparo. N�o somente no commit, mas � uma decis�o do framework. Se voc� desejar for�ar o disparo dos comandos sql, vc deve utilizar o m�todo flush do
		// EntityManager.
		em.getTransaction().commit();

		// Neste momento voc� espera que ocorra um select, mas n�o ocorre!
		// N�o fez o select porque o objeto estava no cache do hibernate.
		Curso curso2 = em.find(Curso.class, "INF");
		System.out.println("curso2=" + curso2);

		// Podemos for�ar a limpeza do cache (isso normalmente n�o � feito!!)
		em.clear();
		Curso curso3 = em.find(Curso.class, "INF");
		System.out.println("curso3=" + curso3);

		// Se o objeto estiver gerenciado pelo Hibernate (em.find(Curso.class, "INF")), as altera��es nos atributos s�o automaticamente atualizadas no banco.
		em.getTransaction().begin();
		System.out.println("Altera��o em objeto gerenciado.");
		curso3.setNome("Engenharia de Software");
		em.getTransaction().commit();

		// Se o objeto n�o estiver sendo gerenciado pelo Hibernate, as altera��es N�O ir�o se refletir no banco
		em.clear(); // limpei todos os objetos do cache, logo nenhum objeto est� sendo gerenciado
		em.getTransaction().begin();
		System.out.println("Altera��o em objeto N�O gerenciado.");
		curso3.setNome("Cajuina"); // a altera��o em um objeto n�o gerenciado � ignorada pelo Hibernate.
		em.getTransaction().commit();

		// Podemos tornar um objeto gerenciado
		em.getTransaction().begin();
		em.merge(curso3);
		System.out.println("Altera��o em objeto que VOLTOU a ser gerenciado.");
		curso3.setNome("Cajuina"); // a altera��o em um objeto n�o gerenciado � ignorada pelo Hibernate.
		em.getTransaction().commit();

		// Remover o objeto do banco
		Curso curso4 = em.find(Curso.class, "INF");
		em.getTransaction().begin();
		em.remove(curso4);
		em.getTransaction().commit();

	}

	private static Logger logger = Logger.getLogger(Exemplo01.class);
}
