package br.ucsal.poo20172.bd2.revisao01.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Curso {

	@Id
	@Column(length = 3)
	private String cod;

	@Column(length = 40)
	private String nome;
	
	// Ao fazer este mapeamento, tornamos a rela��o de Aluno com curso BIDIRECIONAL. Isto � opcional.
	// Um curso pode ter muitos alunos, mas um aluno tem um �nico curso.
	@OneToMany(mappedBy="curso")
	private List<Aluno> alunos;

	public Curso() {
	}

	public Curso(String cod, String nome) {
		super();
		this.cod = cod;
		this.nome = nome;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Curso [cod=" + cod + ", nome=" + nome + "]";
	}

}
