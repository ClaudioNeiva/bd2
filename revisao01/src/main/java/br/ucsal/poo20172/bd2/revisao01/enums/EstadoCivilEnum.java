package br.ucsal.poo20172.bd2.revisao01.enums;

public enum EstadoCivilEnum {

	SOLTEIRO("SLT"), VIUVO("VIV"), OUTROS("OTR"), CASADO("CSD");

	private String cod;

	private EstadoCivilEnum(String cod) {
		this.cod = cod;
	}

	public String getCod() {
		return cod;
	}

	public static EstadoCivilEnum valueOfCod(String cod) {
		for (EstadoCivilEnum estadoCivil : values()) {
			if (estadoCivil.getCod().equalsIgnoreCase(cod)) {
				return estadoCivil;
			}
		}
		throw new IllegalArgumentException();
	}

}
