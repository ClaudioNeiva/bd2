package br.ucsal.poo20172.bd2.revisao01.converters;

import javax.persistence.AttributeConverter;

import br.ucsal.poo20172.bd2.revisao01.enums.EstadoCivilEnum;

public class EstadoCivilConverter implements AttributeConverter<EstadoCivilEnum, String> {

	@Override
	public String convertToDatabaseColumn(EstadoCivilEnum attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getCod();
	}

	@Override
	public EstadoCivilEnum convertToEntityAttribute(String dbData) {
		return EstadoCivilEnum.valueOfCod(dbData);
	}

	// @Override
	// public String convertToDatabaseColumn(EstadoCivilEnum atributo) {
	// if (atributo == null) {
	// return null;
	// }
	// switch (atributo) {
	// case CASADO:
	// return "CSD";
	// case SOLTEIRO:
	// return "SLT";
	// case VIUVO:
	// return "VIV";
	// case OUTROS:
	// return "OTR";
	// default:
	// return null;
	// }
	// }
	//
	// @Override
	// public EstadoCivilEnum convertToEntityAttribute(String dbData) {
	// if (dbData == null) {
	// return null;
	// }
	// if (dbData.equals("CSD")) {
	// return EstadoCivilEnum.CASADO;
	// }
	// if (dbData.equals("SLT")) {
	// return EstadoCivilEnum.SOLTEIRO;
	// }
	// if (dbData.equals("VIV")) {
	// return EstadoCivilEnum.VIUVO;
	// }
	// if (dbData.equals("OTR")) {
	// return EstadoCivilEnum.OUTROS;
	// }
	// return null;
	// }

}
