package br.ucsal.poo20172.bd2.revisao01.exemplo_log;

import org.apache.log4j.Logger;

public class Caju {

	private Logger logger = Logger.getLogger(Caju.class);

	public Caju(String nome) {
		// logar a cria��o do objeto (info ou debug)
		logger.info("Criei um objeto caju, cujo nome � " + nome + ".");
	}

	public void metodo1(Integer valor) {
		try {
			// logar mais detalhado, utilizado no momento de um debug
			logger.debug("Deu um problema s�rio no metodo1 (valor=" + valor + ").");
			throw new Exception();
		} catch (Exception e) {
			// logar um erro
			logger.error("Deu um problema s�rio no metodo1.", e);
		}
	}

}
