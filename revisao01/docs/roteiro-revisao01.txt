 1. criamos modelo
 2. criamos um projeto java maven
 3. atualizamos a vers�o do java no pom.xml de 1.5 (default) para 1.8
 4. atualizamos o projeto: maven -> update project
 5. incluimos a depend�ncia do hibernate no pom.xml (string pesquisou no google)
 6. verficamos que ao incluir a depend�ncia do hibernate, esta trouxe o javax.persistence (jpa)
 7. adicionamos a vers�o do driver do postgresql compat�vel com a vers�o do postgresql da ucsal
 8. criamos a pasta META-INF no resources
 9. criamos o arquivo persistence.xml na pasta META-INF
10. configuramos o persistence.xml com os par�metros para acesso ao postgres da ucsal
11. configuramos o persistence.xml para criar o banco
12. adicionamos o log4j no pom.xml
13. criamos e configuramos o arquivo log4j.properties no resource
14. falamos da possibilidade de criar um persistence.xml espec�fico para execu��o de testes (/src/test/resources)
15. criamos uma classe para teste da conex�o do hibernate (Exemplo01)
16. criamos uma das classes de dom�nio (Curso) e anotamos com @Entity a classe e @Id o cod
17. refinamos a defini��o dos tamanhos para os atributos de Curso
18. manipulamos dados na entidade para que o hibernate crie os comandos DML pra gente (insert, update, delete e select) - m�todo manipularCurso(EntityManager em)
19. criamos a entidade Aluno
20. criamos o tratamento da enumera��o

