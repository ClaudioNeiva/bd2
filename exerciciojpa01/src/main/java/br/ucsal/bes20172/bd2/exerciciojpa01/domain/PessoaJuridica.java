package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class PessoaJuridica {

	@Id
	String cnpj;

	@Column(nullable = false, length = 40)
	String nome;

	@OneToMany
	@JoinTable(name = "pessoajuridica_ramosatividade", foreignKey = @ForeignKey(name = "fk_pessoajuridica"), inverseForeignKey = @ForeignKey(name = "ramoatividade"), joinColumns = {
			@JoinColumn(name = "cnpj") }, inverseJoinColumns = @JoinColumn(name = "ramo_id"))
	List<RamoAtividade> ramosAtividade;

	@Column(nullable = false, precision = 10, scale = 2)
	BigDecimal faturamento;

	@ManyToMany
	@JoinTable(name = "vendedor_cliente", foreignKey = @ForeignKey(name = "fk_vendedor_cliente_vendedor"), inverseForeignKey = @ForeignKey(name = "fk_vendedor_cliente_pessoajuridica"), joinColumns = {
			@JoinColumn(name = "vendedor") }, inverseJoinColumns = @JoinColumn(name = "cliente"))
	List<Vendedor> vendedores;

}
