package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Cidade {

	@Id
	@Column(length = 3)
	String sigla;

	@Column(length = 40)
	String nome;

	@ManyToOne(optional = false)
	Estado estado;

}
